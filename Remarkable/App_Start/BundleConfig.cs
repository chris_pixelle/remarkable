﻿using System.Web;
using System.Web.Optimization;

namespace Remarkable
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Vendor/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/Vendor/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/upload").Include(
                        "~/Scripts/ang/AngApp.js",
                        "~/Scripts/ang/UploadController.js"));

            bundles.Add(new ScriptBundle("~/bundles/frontendjs").Include(
                        "~/Scripts/pres-contact.js",
                        "~/Scripts/p-frontend.js"));

            bundles.Add(new ScriptBundle("~/bundles/tags").Include(
                        "~/Scripts/admin-tags.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Vendor/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Vendor/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/frontendcss").Include(
                      "~/Content/style.css"));
        }
    }
}
