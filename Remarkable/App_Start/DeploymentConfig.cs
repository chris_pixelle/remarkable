﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace Remarkable
{
    public class DeploymentConfig
    {
        private static bool HasRunRunIdentityUpdates;

        public static void RunUpdates()
        {
            RunUpdates(true);
        }

        public static void RunIdentityUpdates()
        {
            if (HasRunRunIdentityUpdates) return;
            RunUpdates(false);
            HasRunRunIdentityUpdates = true;
        }

        public static void RunUpdates(bool startup)
        {
            RunUpdates01(startup);
        }
        

        private static void RunUpdates01(bool startup)
        {
            if(startup)
            { 
                DeploymentService.RunDeploymentInit();
                using (var _db = new dbDataContext())
                {
                    var pages = _db.Contents.ToList();
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.HomePagePromo))
                    {
                        var c = new Content()
                        {
                            Title = "One",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        c = new Content()
                        {
                            Title = "Two",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        c = new Content()
                        {
                            Title = "Three",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        c = new Content()
                        {
                            Title = "Four",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }

                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.HomePagePromo))
                    {
                        var c = new Content()
                        {
                            Title = "One",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Large_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        c = new Content()
                        {
                            Title = "Two",
                            SystemType = Content.SystemTypeOption.HomePagePromo,
                            Type = Content.TypeOption.Home_Page_Large_Promo,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.About))
                    {
                        var c = new Content()
                        {
                            Title = "About Us",
                            SystemType = Content.SystemTypeOption.About,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.Contact))
                    {
                        var c = new Content()
                        {
                            Title = "Contact Us",
                            SystemType = Content.SystemTypeOption.Contact,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.Careers))
                    {
                        var c = new Content()
                        {
                            Title = "Careers",
                            SystemType = Content.SystemTypeOption.Careers,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.Health))
                    {
                        var c = new Content()
                        {
                            Title = "Health",
                            SystemType = Content.SystemTypeOption.Health,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.MetalDeck))
                    {
                        var c = new Content()
                        {
                            Title = "MetalDeck",
                            SystemType = Content.SystemTypeOption.MetalDeck,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.StudWelding))
                    {
                        var c = new Content()
                        {
                            Title = "StudWelding",
                            SystemType = Content.SystemTypeOption.StudWelding,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                    if (!pages.Any(x => x.SystemType == Content.SystemTypeOption.Home))
                    {
                        var c = new Content()
                        {
                            Title = "Home",
                            SystemType = Content.SystemTypeOption.Home,
                            Type = Content.TypeOption.Site_Content,
                            CreateDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        c.TitleUrl = c.Title.ToSafeString();
                        _db.Contents.InsertOnSubmit(c);
                        _db.SubmitChanges();
                    }
                }
            }
            else
            {
                DeploymentService.AddAccount("RunUpdates01", "admin@Remarkable.co.nz", "MEJb3if9m8n2FuRPPHVK");
            }
        }
        
    }
}