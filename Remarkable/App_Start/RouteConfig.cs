﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Remarkable
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Products",
                url: "Products/{id}",
                defaults: new { controller = "Products", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );

            routes.MapRoute(
                name: "Brands",
                url: "Brands/{id}",
                defaults: new { controller = "Brands", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );

            routes.MapRoute(
                name: "Contact",
                url: "Contact/{id}",
                defaults: new { controller = "Home", action = "Contact", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );

            routes.MapRoute(
                name: "Services",
                url: "Services/{id}",
                defaults: new { controller = "Home", action = "Services", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );

            routes.MapRoute(
                name: "About",
                url: "About/{id}",
                defaults: new { controller = "Home", action = "About", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Remarkable.Controllers" }
            );
        }
    }
}
