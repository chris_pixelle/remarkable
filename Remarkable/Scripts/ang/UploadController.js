﻿myApp.controller('UploadController', ['$scope', '$http', 'Upload', '$attrs', function ($scope, $http, Upload, $attrs) {
    if (!$attrs.siterestype) throw new Error("No siterestype for UploadController");

    $scope.ticket = {
        SiteResourceType: $attrs.siterestype,
        PreviewUrl: $attrs.previewurl,
        FileName: $attrs.filename,
        Code: $attrs.code,
        IsActive: $attrs.isactive == "True",
        ObjectType: $attrs.objecttype
    };

    if ($scope.ticket.PreviewUrl)
        $scope.job = $scope.ticket;

    $scope.$watch('files', function () {
        $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    $scope.log = '';
    $scope.updloading = false;

    $scope.upload = function (files) {
        if (!(files && files.length)) 
            return;
        
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            console.log("file - " + file.filaname);
            $scope.updloading = true;
            $scope.errormsg = null;
            if (!file.$error) {
                Upload.upload({
                    url: '/admin/util/uploadbinaryasset',
                    data: {
                        ticket: $scope.ticket,
                        file: file
                    }
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.progwidth = progressPercentage + "%";
                    $scope.log = 'progress: ' + progressPercentage + '% ' +
                                evt.config.data.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    if (data.success)
                    {
                        $scope.job = data.job;
                        $scope.job.IsActive = true;
                        //setTimeout(function () {checkForNexts();}, 500);
                    }
                    else
                    {
                        $scope.errormsg = data.error;
                    }
                    $scope.updloading = false;
                    $scope.files = null;
                    $scope.log = "";
                });
            }
        }
        
    };

    $scope.hasPreviewUrl = function () {
        return $scope.job || $scope.ticket.PreviewUrl;
    };

    $scope.hasPreviewImg = function () {
        return $scope.getPreviewUrl().split("|")[0] == "img";
    };

    $scope.hasPreviewFile = function () {
        return $scope.getPreviewUrl().split("|")[0] == "file";
    };
    
    $scope.getPreviewShowData = function () {
        return $scope.getPreviewUrl().split("|")[1];
    };

    $scope.getPreviewDownload = function () {
        return $scope.getPreviewUrl().split("|")[2];
    };

    $scope.getPreviewUrl = function () {
        return $scope.job ? $scope.job.PreviewUrl : $scope.ticket.PreviewUrl;
    };

    $scope.delete = function () {

        if (confirm("Are you sure you want to delete this file?"))
        {
            $http.post("/admin/util/deletebinaryasset", { ticket: ($scope.job ? $scope.job : $scope.ticket) }).
            success(function (data, status, headers, config) {
                $scope.job = null;
                $scope.ticket = data.ticket;
                //setTimeout(function () { checkForNexts(); }, 500);
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/home/suburbs/");

            });
        }
    };

}]);