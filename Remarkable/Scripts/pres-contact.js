﻿$(function () {

    /* Contact Form */

    $(".contactformbox").on("click", ".contactbtn", function () {
        var model = {
            Name: $("#Name").val(),
            Email: $("#Email").val(),
            Phone: $("#Phone").val(),
            Comments: $('#Project').val() + " <BR><BR>" + $("#Comments").val(),
            //Recaptcha: grecaptcha.getResponse()
        };

        $(this).val("Sending..");
        /*
        var cpt = $(".g-recaptcha")[0];
        $(cpt).remove();
        */
        var post = JSON.stringify(model);
        $.ajax(
            {
                type: "POST",
                url: "/Home/ContactRequest/",
                data: post,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $(".contact-form").html(result);

                    if ($(".contact-form .g-recaptcha").length > 0) {
                        $(".contact-form .g-recaptcha").replaceWith(cpt);
                    }
                    
                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push({
                        'event': 'contactSuccess',
                        'name': model.Name
                    });
                    // Rest of the success callback code

                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                }
            });
    });

});
