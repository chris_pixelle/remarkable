﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Areas.Admin.Controllers
{
    public class BrandsController : AdminSiteController
    {
        public const string OBJECT_NAME = "Brand";

        // GET: Admin/Brands
        public ActionResult Index(AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                return View(_db.Brands.ToList());
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Brand>(o => o.SiteResources);
                var model = id.HasValue ? _db.Brands.SingleOrDefault(x => x.ID == id) : new Models.Brand() { };
                model.Resources = model.SiteResources.ToList();
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int? id, Brand model)
        {
            using (var _db = new dbDataContext())
            {
                ResourceService.SavePrep(ModelState, model, _db);

                var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                if (ModelState.IsValid)
                {
                    var saveModel = _db.Brands.SingleOrDefault(x => x.ID == id) ?? new Models.Brand()
                    {
                        CreateDate = DateTime.Now
                    };

                    saveModel.Name = model.Name;
                    saveModel.NameUrl = model.Name.EncodeToUrl();
                    saveModel.Title = model.Title;
                    saveModel.Description = model.Description;
                    saveModel.MetaDescription = model.MetaDescription;
                    saveModel.OrderNo = model.OrderNo;
                    saveModel.IsHidden = model.IsHidden;

                    if (model.ID == 0)
                    {
                        _db.Brands.InsertOnSubmit(saveModel);
                    }

                    _db.SubmitChanges();
                    model.ID = saveModel.ID;

                    saveModel.Description = ResourceService.SaveCommit(model, saveModel, _db).RunReplaces(saveModel.Description);
                    
                    _db.SubmitChanges();

                    return RedirectToAction("Edit", new { id = model.ID,  msg = !id.HasValue ? AppMessageOption.AddSuccess : AppMessageOption.EditSuccess });
                }
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Delete(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            ViewBag.MsgErr = true;
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Brand>(o => o.SiteResources);
                var model = _db.Brands.Single(x => x.ID == id);
                model.Resources = model.SiteResources.ToList();
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Delete(int? id, Brand model)
        {
            using (var _db = new dbDataContext())
            {
                if (_db.Products.Any(x => x.BrandID == id))
                    return RedirectToAction("Delete", new { id = model.ID, msg = AppMessageOption.Accosiated_Products_Exist });
                var options = new DataLoadOptions();
                options.LoadWith<Brand>(o => o.SiteResources);
                var saveModel = _db.Brands.Single(x => x.ID == id);
                saveModel.SiteResources.ToList().ForEach(x => ResourceService.Delete(x));
                _db.Brands.DeleteOnSubmit(saveModel);
                _db.SubmitChanges();
                return RedirectToAction("Index", new {  msg = AppMessageOption.Deleted });
            }

        }
    }
}