﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;


namespace Remarkable.Areas.Admin.Controllers
{
    public class ContentController : AdminSiteController
    {
        public const string OBJECT_NAME = "Content";
        
        // GET: Admin/Contents
        public ActionResult Index(Content.TypeOption? id)
        {
            ViewBag.Title = "Pages";
            if(id == Models.Content.TypeOption.News)
                ViewBag.AllowAdd = true;

            using (var _db = new dbDataContext())
            {
                return View(id == Models.Content.TypeOption.Home_Page_Banner ? "HomePageBanners" : "Index" ,_db.Contents.Where(x => x.TypeE == (int?)id).ToList());
            }
        }

        public ActionResult Promotions()
        {
            ViewBag.Title = "Promotions";
            using (var _db = new dbDataContext())
            {
                return View("Promotions" , _db.Contents.Where(x => x.TypeE == (int?)Models.Content.TypeOption.Home_Page_Promo || x.TypeE == (int?)Models.Content.TypeOption.Home_Page_Large_Promo).ToList());
            }
        }

        public ActionResult News()
        {
            ViewBag.Title = "News";
            ViewBag.AllowAdd = true;
            using (var _db = new dbDataContext())
            {
                return View( "Index", _db.Contents.Where(x =>  x.SystemTypeE == (int?)Models.Content.SystemTypeOption.News).ToList());
            }
        }


        [HttpGet]
        public ActionResult Edit(int? id, Content.TypeOption? type, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                var model = new Models.Content();
                options.LoadWith<Content>(o => o.SiteResources);
                if (id == 0)
                {
                    model = new Models.Content()
                    {
                        Type = Models.Content.TypeOption.Site_Content,
                        SystemType = Models.Content.SystemTypeOption.News,
                        
                        CreateDate = System.DateTime.Now
                    };
                    model.Type = Models.Content.TypeOption.News;
                }
                else
                {
                    model = id.HasValue ? _db.Contents.SingleOrDefault(x => x.ID == id) : new Models.Content() { Type = (Models.Content.TypeOption)type };
                    model.Resources = model.SiteResources.ToList();
                }

                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int? id, Content model)
        {
            using (var _db = new dbDataContext())
            {
                ResourceService.SavePrep(ModelState, model, _db);

                var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                if (ModelState.IsValid)
                {
                    var saveModel = _db.Contents.SingleOrDefault(x => x.ID == id) ?? new Models.Content()
                    {
                        CreateDate = DateTime.Now,
                        Type = model.Type
                    };

                    saveModel.Title = model.Title;
                    saveModel.TitleUrl = model.Title.EncodeToUrl();
                    if(model.CreateDate == default(DateTime))
                    {
                        model.CreateDate = System.DateTime.Now;
                    }
                    saveModel.CreateDate = model.CreateDate;
                    saveModel.Title = model.Title;
                    saveModel.Html1 = model.Html1;
                    saveModel.Html2 = model.Html2;
                    saveModel.Html3 = model.Html3;
                    saveModel.MetaDescription = model.MetaDescription;
                    saveModel.OrderNo = model.OrderNo;
                    saveModel.IsHidden = model.IsHidden;
                    saveModel.ModifiedDate = DateTime.UtcNow;
                    saveModel.SystemType = model.SystemType;
                    if (model.ID == 0)
                    {
                        _db.Contents.InsertOnSubmit(saveModel);
                    }

                    _db.SubmitChanges();
                    model.ID = saveModel.ID;

                    saveModel.Html1 = ResourceService.SaveCommit(model, saveModel, _db).RunReplaces(saveModel.Html1);

                    _db.SubmitChanges();

                    return RedirectToAction("Edit", new { id = model.ID, msg = !id.HasValue ? AppMessageOption.AddSuccess : AppMessageOption.EditSuccess });
                }
                return View(model);
            }
        }
    }
}