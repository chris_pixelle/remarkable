﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Areas.Admin.Controllers
{
    public class UtilController : AdminSiteController
    {
        // GET: Admin/Util
        [HttpPost]
        public ActionResult UploadBinaryAsset(SiteResourceTicket ticket)
        {
            HttpPostedFileBase file = Request.Files[0];
            if (ticket.Code == Guid.Empty)
                ticket.Code = Guid.NewGuid();

            var result = ResourceService.SaveJob(file, ticket);

            return this.Json(result);
        }

        [HttpPost]
        public ActionResult DeleteBinaryAsset(SiteResourceTicket ticket)
        {
            using (var _db = new dbDataContext())
            {
                var hits = _db.SiteResources.Where(x => x.Code == ticket.Code).ToList();
                hits.ForEach(x =>
                {
                    ResourceService.Delete(x);
                });

                ResourceService.Delete(ticket);

                return this.Json(new { success = true, ticket = (ISavedFile)new SiteResourceTicket() { SiteResourceType = ticket.SiteResourceType, ObjectType = ticket.ObjectType } });
            }
        }
    }
}