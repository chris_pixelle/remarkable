﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Areas.Admin.Controllers
{
    public class TagsController : AdminSiteController
    {
        public const string OBJECT_NAME = "Tag";

        // GET: Admin/Tags
        public ActionResult Index(AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                return View(_db.Tags.ToList());
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Tag>(o => o.SiteResources);
                var model = id.HasValue ? _db.Tags.SingleOrDefault(x => x.ID == id) : new Models.Tag() { };
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int? id, Tag model)
        {
            using (var _db = new dbDataContext())
            {
                if (ModelState.IsValid)
                {
                    var saveModel = _db.Tags.SingleOrDefault(x => x.ID == id) ?? new Models.Tag()
                    {
                        //CreateDate = DateTime.Now
                    };

                    saveModel.Name = model.Name;
                    saveModel.NameUrl = model.Name.EncodeToUrl();

                    if (model.ID == 0)
                    {
                        _db.Tags.InsertOnSubmit(saveModel);
                    }

                    _db.SubmitChanges();
                    model.ID = saveModel.ID;

                    _db.SubmitChanges();

                    return RedirectToAction("Edit", new { id = model.ID, msg = !id.HasValue ? AppMessageOption.AddSuccess : AppMessageOption.EditSuccess });
                }
                return View(model);
            }
        }


        [HttpGet]
        public ActionResult Delete(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            ViewBag.MsgErr = true;
            using (var _db = new dbDataContext())
            {
                var model = _db.Tags.Single(x => x.ID == id);
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Delete(int? id, Tag model)
        {
            using (var _db = new dbDataContext())
            {
                 var saveModel = _db.Tags.Single(x => x.ID == id);
                _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.TagID == id));
                _db.Tags.DeleteOnSubmit(saveModel);
                _db.SubmitChanges();
                return RedirectToAction("Index", new { msg = AppMessageOption.Deleted });
            }

        }
    }
}