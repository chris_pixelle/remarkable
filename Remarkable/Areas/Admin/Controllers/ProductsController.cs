﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;
using Remarkable.Models;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Areas.Admin.Controllers
{
    public class ProductsController : AdminSiteController
    {
        public const string OBJECT_NAME = "Product";

        // GET: Admin/Products
        public ActionResult Index(AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Product>(o => o.Brand);
                _db.LoadOptions = options;
                return View(_db.Products.ToList().OrderBy(p=>p.Price));
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Product>(o => o.SiteResources);
                options.LoadWith<Product>(o => o.Tag_Products);
                options.LoadWith<Tag_Product>(o => o.Tag);
                var model = id.HasValue ? _db.Products.SingleOrDefault(x => x.ID == id) : new Models.Product() { };
                model.Resources = model.SiteResources.ToList();
                model.Tags = model.GetTags();
                ViewBag.Brands = _db.Brands.OrderBy(x => x.Name).ToList();
                ViewBag.Tags = _db.Tags.ToList();
                model.BrandID = 1;
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int? id, Product model)
        {
            using (var _db = new dbDataContext())
            {
                ViewBag.Tags = _db.Tags.ToList();
                ResourceService.SavePrep(ModelState, model, _db);
                model.Tags = Request["TagIDs"] == null ? new List<Tag>() : Request["TagIDs"].Split(',').Select(s => new Tag() { Name = ((List<Tag>)ViewBag.tags).Where(tg => tg.ID == int.Parse(s)).Single().Name, ID = int.Parse(s) }).ToList();

                var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                if (ModelState.IsValid)
                {
                    var saveModel = _db.Products.SingleOrDefault(x => x.ID == id) ?? new Product()
                    {
                        CreateDate = DateTime.Now
                    };

                    saveModel.Name = model.Name;
                    saveModel.NameUrl = model.Name.EncodeToUrl();
                    saveModel.Description = model.Description;
                    saveModel.MetaDescription = model.MetaDescription;
                    saveModel.OrderNo = model.OrderNo;
                    saveModel.Price = model.Price;
                    saveModel.Details = model.Details;
                    saveModel.Specifications = model.Specifications;
                    saveModel.BrandID = model.BrandID;
                    saveModel.IsHidden = model.IsHidden;

                    if (model.ID == 0)
                    {
                        _db.Products.InsertOnSubmit(saveModel);
                    }

                    _db.SubmitChanges();
                    model.ID = saveModel.ID;

                    saveModel.Description = ResourceService.SaveCommit(model, saveModel, _db).RunReplaces(saveModel.Description);

                    _db.SubmitChanges();

                    var delTags = from o in _db.Tag_Products
                                  where o.ProductID == saveModel.ID
                                  select o;
                    _db.Tag_Products.DeleteAllOnSubmit(delTags);
                    _db.SubmitChanges();

                    if (model.Tags != null && model.Tags.Count() > 0)
                    {
                        List<Tag_Product> tags = model.Tags.Select(t => new Tag_Product() { ProductID = saveModel.ID, TagID = t.ID }).ToList();
                        _db.Tag_Products.InsertAllOnSubmit(tags);
                        _db.SubmitChanges();
                    }

                    return RedirectToAction("Edit", new { id = model.ID, msg = !id.HasValue ? AppMessageOption.AddSuccess : AppMessageOption.EditSuccess });
                }
                ViewBag.Brands = _db.Brands.OrderBy(x => x.Name).ToList();
                return View(model);
            }
        }


        [HttpGet]
        public ActionResult Delete(int? id, AppMessageOption? msg)
        {
            ViewBag.Msg = AppMessageService.GetMessage(msg, OBJECT_NAME);
            ViewBag.MsgErr = true;
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Product>(o => o.SiteResources);
                var model = _db.Products.Single(x => x.ID == id);
                model.Resources = model.SiteResources.ToList();
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Delete(int? id, Product model)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Product>(o => o.SiteResources);
                var saveModel = _db.Products.Single(x => x.ID == id);
                _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.ProductID == id));
                saveModel.SiteResources.ToList().ForEach(x => ResourceService.Delete(x));
                _db.Products.DeleteOnSubmit(saveModel);
                _db.SubmitChanges();
                return RedirectToAction("Index", new { msg = AppMessageOption.Deleted });
            }

        }
    }
}