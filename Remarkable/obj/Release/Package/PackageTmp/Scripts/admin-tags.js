﻿$(function () {

    $(".addtag").click(function () {
        var container = $(this).parents(".tagsdiv");
        var sel = container.find(".tagsel");

        var tag = {
            name: sel.find("option:selected").text(),
            ID: sel.find("option:selected").val()
        };

        if (tag.ID == "")
            return;

        $(container).find(".taglist").append($("<span class='tag' id='taglabel_" + tag.ID + "'>" + tag.name + " <a href='javascript:void(0)' class='removetag' data-tagid='" + tag.ID + "'>x</a></span>"));
        $(container).find(".taglist").append($("<input type='hidden' id='tag_" + tag.ID + "' name='TagIDs' value='" + tag.ID + "' />"));
    });

    $(".taglist").on("click", ".removetag", function () {
        var tagid = $(this).attr("data-tagid");
        $("#taglabel_" + tagid).remove();
        $("#tag_" + tagid).remove();
    });

});