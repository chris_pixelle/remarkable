﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Remarkable.Models;
using System.Data.Linq;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Controllers
{
    public class BrandsController : PublicController
    {
        // GET: Brand
        public ActionResult Index(string id)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Brand>(o => o.SiteResources);
                options.LoadWith<Brand>(o => o.Products);
                options.LoadWith<Product>(o => o.SiteResources);
                options.LoadWith<Product>(o => o.Tag_Products);
                options.LoadWith<Tag_Product>(o => o.Tag);
                _db.LoadOptions = options;
                return View(_db.Brands.Single(x => x.NameUrl == id));
            }
        }
    }
}