﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Remarkable.Models.Services;

namespace Remarkable.Controllers
{
    public class SiteController : Controller
    {
        protected SiteResourceService _SiteResService;
        public SiteResourceService ResourceService
        {
            get
            {
                if (_SiteResService == null)
                    _SiteResService = new SiteResourceService();
                return _SiteResService;
            }
        }
    }
}