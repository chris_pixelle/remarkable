﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Remarkable.Models;
using System.Data.Linq;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Controllers
{
    public class ProductsController : PublicController
    {
        // GET: Products
        public ActionResult Index(string id)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Product>(o => o.SiteResources);
                options.LoadWith<Product>(o => o.Tag_Products);
                options.LoadWith<Tag_Product>(o => o.Tag);
                options.LoadWith<Product>(o => o.Brand);
                _db.LoadOptions = options;
                return View(_db.Products.Single(x => x.NameUrl == id));
            }
        }
    }
}