﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Remarkable.Models;
using System.Data.Linq;
using System.Configuration;
using System.Net.Mail;
using Remarkable.Models.Services;
using Remarkable.Models.Extensions;

namespace Remarkable.Controllers
{
    public class HomeController : PublicController
    {
        public ActionResult Index()
        {
            DeploymentConfig.RunIdentityUpdates();
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                options.LoadWith<Product>(o => o.SiteResources);
                options.LoadWith<Product>(o => o.Tag_Products);
                options.LoadWith<Tag_Product>(o => o.Tag);
                _db.LoadOptions = options;
                return View(new HomePageViewModel()
                    {
                        Banners = _db.Contents.Where(x => x.TypeE == (int)Models.Content.TypeOption.Home_Page_Banner && !x.IsHidden).ToList(),
                        Products = _db.Products.Where(x => !x.IsHidden).ToList().OrderBy(x => Guid.NewGuid()).ToList(),
                        Promotions = _db.Contents.Where(x => x.TypeE == (int)Models.Content.TypeOption.Home_Page_Promo && !x.IsHidden).ToList(),
                        Large_Promotions = _db.Contents.Where(x => x.TypeE == (int)Models.Content.TypeOption.Home_Page_Large_Promo && !x.IsHidden).ToList(),
                } 
                );
            }
        }
        public ActionResult News()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("News", _db.Contents.Where(x => x.SystemTypeE == (int)Models.Content.SystemTypeOption.News).ToList());
            }
        }
        public ActionResult ViewNews(int id)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("News", _db.Contents.Where(x => x.ID == id).ToList());
            }
        }

        public ActionResult Services()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("Content",_db.Contents.Single(x => x.SystemTypeE == (int)Models.Content.SystemTypeOption.StudWelding));
            }
        }

        public ActionResult Page(string id)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("Content", _db.Contents.Single(x => x.TitleUrl == id));
            }
        }


        public ActionResult About()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("Content", _db.Contents.Single(x => x.SystemTypeE == (int)Models.Content.SystemTypeOption.About));
            }
        }

        public ActionResult Contact()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Content>(o => o.SiteResources);
                _db.LoadOptions = options;
                return View("Content", _db.Contents.Single(x => x.SystemTypeE == (int)Models.Content.SystemTypeOption.Contact));
            }
        }

        [HttpPost]
        public ActionResult ContactRequest(ContactModel model)
        {
            /*
            var captyaResult = CaptchaService.CheckCaptyaSuccess(model.Recaptcha);
            if (!captyaResult.Success)
            {
                ModelState.AddModelError("", captyaResult.Message);
            }
            */
            if (ModelState.IsValid)
            {
                var mail = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["ContactEmail"])
                {
                    Subject = "Contact from the Remarkable website",
                    IsBodyHtml = true,
                    Body = "Name:" + model.Name + "<br/>" +
                            "Email:" + model.Email + "<br/>" +
                           // "Contact phone no:" + model.Phone + "<br/>" +
                            "Comment:" + model.Comments
                };
                mail.ReplyToList.Add(model.Email);

                EmailSendService.Send(mail);
                return PartialView("_ContactRequestDone");
            }

            return PartialView("_ContactRequest");
        }
    }
}