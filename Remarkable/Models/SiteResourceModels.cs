﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using Remarkable.Models.Services;

namespace Remarkable.Models
{
    public partial class SiteResource : IOrderedObject, ISavedFile
    {
        public enum SiteResourceTypeOption
        {
            Billboard_Desktop = 0,
            Billboard_Mobile = 1,
            Content = 2,
            Product_Image = 3,
            File_For_Download = 4,
        }

        public enum SiteResourceVersion
        {
            ITEM_PAGE,
            ITEM_PAGE_RETINA,
            ITEM_LIST,
            ITEM_LIST_RETINA,
            ITEM_LARGE,
            ITEM_LARGE_RETINA,
        }


        public SiteResourceTypeOption SiteResourceType
        {
            get
            {
                return (SiteResourceTypeOption)(SiteResourceTypeE ?? 0);
            }
            set
            {
                SiteResourceTypeE = (int)value;
            }
        }

        public SiteResourceOwner.ObjectTypeOption ObjectType
        {
            get
            {
                return (SiteResourceOwner.ObjectTypeOption)(ObjectTypeE ?? 0);
            }
            set
            {
                ObjectTypeE = (int)value;
            }
        }

        public string SavePath
        {
            get
            {
                return "~/Content/SiteResources/" + ObjectType.ToString() + "/" + ObjectID.ToString() + "/" + Code + "/";
            }
        }

        //START VIEW MEMBERS

        public string PreviewUrl
        {
            get
            {
                return SiteResource.getPreview(SavePath, FileName, SiteResourceType);
            }
        }

        private string _downloadUrl;
        public string DownloadUrl
        {
            get
            {
                if (_downloadUrl == null)
                    _downloadUrl = getUrl();
                return _downloadUrl;
            }
        }

        public bool IsActive { get { return false; } set { } }
        public int Index { get; set; }
        public bool CollectCaption { get { return getCollectCaption(SiteResourceType); } }

        public string action { get; set; }
        public string url { get; set; }
        public string urlLessFile { get; set; }

        public string getFileImage()
        {
            var img = "text-x-generic-icon.png";
            if (FileName.EndsWith(".pdf")) { img = "adobe_pdf.png"; }
            if (FileName.EndsWith(".zip")) { img = "mac_zip_icon.jpg"; }
            return img;
        }

        public string getDownloadName()
        {
            return getExtension().ToUpper() + " " + getFileSize();
        }

        public string getDownloadName2()
        {
            return FileName + " " + getFileSize();
        }

        public string getFileSize()
        {
            if(FileSize.HasValue)
                return "(" + ((float)((FileSize / 1024f) / 1024f)).ToString("0.0") + " mb)";
            return "";
        }

        public string getExtension()
        {
            return Path.GetExtension(FileName).TrimStart('.');
        }

        public string getUrl(SiteResourceVersion version = SiteResourceVersion.ITEM_PAGE, bool isRetina = false)
        {

            return SiteResourceService.getUrl(this, version, isRetina);
        }

        public string getUrlOrFile(SiteResourceVersion version, bool isRetina = true)
        {
            var ext = Path.GetExtension(FileName).ToLower();
            if (ext == (".png") || ext == (".gif") || ext == (".jpeg") || ext == (".jpg"))
                return urlLessFile + SiteResourceService.getFileNameVersion(FileName, version, isRetina);
            else
                return "/content/images/" + getFileImage();
        }

       

        public static string getPreview(string SavePath, string FileName, SiteResource.SiteResourceTypeOption type)
        {
            var savePath = SavePath.TrimStart('~');
            string imagepath;
            string downloadpath;
            switch (type)
            {
                case SiteResource.SiteResourceTypeOption.Billboard_Desktop:
                case SiteResource.SiteResourceTypeOption.Billboard_Mobile:
                case SiteResource.SiteResourceTypeOption.Content:
                case SiteResource.SiteResourceTypeOption.Product_Image:
                    imagepath = savePath + FileName;
                    return string.Format("{0}|{1}|{2}", "img", imagepath, imagepath);
                default:
                    downloadpath = savePath + FileName;
                    return string.Format("{0}|{1}|{2}", "file", FileName, downloadpath);
            }
        }

        public static bool getCollectCaption(SiteResource.SiteResourceTypeOption type)
        {
            switch (type)
            {
                /*
                case SiteResource.SiteResourceTypeOption.Other:
                    return true;
                    */
                default:
                    return false;
            }
        }

        public bool isImage()
        {
            return new List<string> { ".png", ".gif", ".jpg", ".jpeg" }.Any(x => x == Path.GetExtension(FileName).ToLower());
        }

        public bool isPortrait()
        {
            if (Width.HasValue)
            {
                return Width < Height;
            }
            return false;
        }

        public SiteResource clone(int ObjectID, SiteResourceOwner.ObjectTypeOption type)
        {
            var sR = (SiteResource)this.MemberwiseClone();
            sR.ID = 0;
            sR.ObjectID = ObjectID;
            sR.ObjectType = type;
            return sR;
        }
    }


    public interface ISavedFile
    {
        string FileName { get; set; }
        string Caption { get; set; }
        Guid Code { get; set; }
        SiteResource.SiteResourceTypeOption SiteResourceType { get; set; }
        SiteResourceOwner.ObjectTypeOption ObjectType { get; set; }
        string SavePath { get; }
        string PreviewUrl { get; }
        string DownloadUrl { get; }
        bool IsActive { get; set; }
        int Index { get; set; }
        bool CollectCaption { get; }
    }

    public class SiteResourceTicket : ISavedFile
    {
        public Guid Code { get; set; }
        public SiteResource.SiteResourceTypeOption SiteResourceType { get; set; }
        public SiteResourceOwner.ObjectTypeOption ObjectType { get; set; }
        public string FileName { get; set; }
        public string Caption { get; set; }
        public bool IsActive { get; set; }
        public int Index { get; set; }
        public string SavePath
        {
            get
            {
                return Code != Guid.Empty ? "/Content/SiteResources/Scratch/" + Code + "/" : null;
            }
        }

        public string PreviewUrl
        {
            get
            {
                if (SavePath == null)
                    return null;

                return SiteResource.getPreview(SavePath,FileName,SiteResourceType);
            }
        }

        public string PreviewUrlLinkOnly
        {
            get
            {
                if (PreviewUrl == null)
                    return null;

                return PreviewUrl.Split('|')[2];
            }
        }

        public string DownloadUrl
        {
            get
            {
                return null;
            }
        }

        public bool CollectCaption { get { return SiteResource.getCollectCaption(SiteResourceType); } }
    }

    public abstract class SiteResourceOwner
    {
        public int CurrentIndex;
        public int? CurrentMultiImageIndex = null;

        public enum ObjectTypeOption
        {
            Content,
            Brand,
            Product
        }

        public List<SiteResource> Resources { get; set; }
        public List<SiteResourceTicket> ResourceTickets { get; set; }

        public abstract int GetObjectID();
        public abstract ObjectTypeOption GetObjectType();
        public abstract List<SiteResource> GetResources();

        public string getPostObjectName()
        {
            return this.GetType().Name.ToLower();
        }

        public bool hasSrUrl(SiteResource.SiteResourceTypeOption option, SiteResource.SiteResourceVersion version)
        {
            if (Resources.Any(p => p.SiteResourceType == option))
            {
                var file = Resources.Where(p => p.SiteResourceType == option).First();
                return true;
            }
            return false;
        }

        public bool getSrIsPortrait(SiteResource.SiteResourceTypeOption option, SiteResource.SiteResourceVersion version)
        {
            if (Resources.Any(p => p.SiteResourceType == option))
            {
                var file = Resources.Where(p => p.SiteResourceType == option).First();
                return file.isPortrait();
            }
            return true;
        }

        public ISavedFile getFile(SiteResource.SiteResourceTypeOption type, int? index = null)
        {
            if (!index.HasValue)
                index = CurrentIndex++;

            if (ResourceTickets == null) ResourceTickets = new List<SiteResourceTicket>();
            if (Resources == null) Resources = new List<SiteResource>();
            ISavedFile sf = null;
            if (!Services.SiteResourceService.IsOnePerType(type))
            {
                var multiImages = ResourceTickets.Where(x => x.SiteResourceType == type).Select(x => (ISavedFile)x).ToList();
                multiImages.AddRange(Resources.Where(x => x.SiteResourceType == type).Select(x => (ISavedFile)x).ToList());
                if (!CurrentMultiImageIndex.HasValue)
                {
                    sf = multiImages.FirstOrDefault() ?? (ISavedFile)new SiteResourceTicket()
                    {
                        SiteResourceType = type,
                        ObjectType = GetObjectType()
                    };
                    CurrentMultiImageIndex = 0;
                }
                else
                {
                    CurrentMultiImageIndex++;
                    sf = multiImages.Skip((int)CurrentMultiImageIndex).FirstOrDefault() ?? (ISavedFile)new SiteResourceTicket()
                    {
                        SiteResourceType = type,
                        ObjectType = this.GetObjectType()
                    };
                }
            }
            else
            {
                sf = (ISavedFile)ResourceTickets.SingleOrDefault(x => x.SiteResourceType == type) ?? (ISavedFile)Resources.FirstOrDefault(x => x.SiteResourceType == type) ?? (ISavedFile)new SiteResourceTicket()
                {
                    SiteResourceType = type,
                    ObjectType = this.GetObjectType()
                };
            }
            /*
            if(this is StolenProperty)
                sf.PostFix = "_STOLEN";
            if (((type == SiteResource.SiteResourceTypeOption.RECEIPT_FOR_PROPERTY &&
                type < SiteResource.SiteResourceTypeOption.EXTRA_PICTURE)) && this is MarkedForSaleProperty)
                sf.PostFix = "_STOLEN";
                */
            sf.Index = (int)index;
            return sf;
        }

        public ISavedFile getFile(SiteResource.SiteResourceTypeOption type, int typeIndex, int ownerIndex)
        {
            var files = getFiles(type);
            if (typeIndex >= files.Count)
                return (ISavedFile)new SiteResourceTicket() { SiteResourceType = type, ObjectType = this.GetObjectType(), Index = ownerIndex };
            else
            {
                files[typeIndex].Index = ownerIndex;
                return files[typeIndex];
            }
        }

        public List<ISavedFile> getFiles(SiteResource.SiteResourceTypeOption type)
        {
            if (ResourceTickets == null) ResourceTickets = new List<SiteResourceTicket>();
            if (Resources == null) Resources = new List<SiteResource>();

            var returnSet = ResourceTickets.Where(x => x.SiteResourceType == type).Select(x => (ISavedFile)x).ToList();
            returnSet.AddRange(Resources.Where(x => x.SiteResourceType == type).Select(x => (ISavedFile)x).ToList());

            return returnSet;
        }

        /*
        public static SiteResourceOwner getSimpleObject(SiteResource sr)
        {
            switch (sr.ObjectType)
            {
                case ObjectTypeOption.PAGE :
                    return new Page() { ID = sr.ObjectID };
                    
                case ObjectTypeOption.PRODUCT:
                    return new Product() { ID = sr.ObjectID };

                case ObjectTypeOption.INGREDIENT:
                    return new Ingredient() { ID = sr.ObjectID };
                    
            }
            return null;
        }
        */
    }

    public class SiteResourceContentReplacer
    {
        private Dictionary<string, string> _replaces = new Dictionary<string, string>();

        public void AddReplace(string from, string to)
        {
            _replaces.Add(from, to);
        }

        public string RunReplaces(string content)
        {
            if (content == null)
                return null;

            foreach (var item in _replaces)
            {
                content = content.Replace(item.Key, item.Value);
            }

            return content;
        }
    }
}