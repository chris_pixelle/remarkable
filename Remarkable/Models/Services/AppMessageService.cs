﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remarkable.Models.Services
{
    public class AppMessageService
    {
        public static string GetMessage(AppMessageOption? msg, string objectName)
        {
            if (!msg.HasValue)
                return null;

            switch (msg)
            {
                case AppMessageOption.AddSuccess:
                    return string.Format("{0} has been added", objectName);
                case AppMessageOption.EditSuccess:
                    return string.Format("{0} has been updated", objectName);
                case AppMessageOption.Deleted:
                    return string.Format("{0} has been deleted", objectName);
                case AppMessageOption.MovedUp:
                    return string.Format("{0} has been moved up in the list", objectName);
                case AppMessageOption.MovedDown:
                    return string.Format("{0} has been moved down in the list", objectName);
                case AppMessageOption.Accosiated_Products_Exist:
                    return string.Format(" Sorry products are still associated with this {0}", objectName);
            }
            return null;
        }
    }

    public enum AppMessageOption
    {
        AddSuccess,
        EditSuccess,
        Deleted,
        MovedUp,
        MovedDown,
        Accosiated_Products_Exist
    }
}