﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using Remarkable.Models.Extensions;
using Simple.ImageResizer;
/*
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
*/
using System.Net;

//Large Files
//https://codeonaboat.wordpress.com/2011/04/22/uploading-a-file-to-amazon-s3-using-an-asp-net-mvc-application-directly-from-the-users-browser/
//http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTForms.html
//http://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-examples-using-sdks.html
//http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-post-example.html
//http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-UsingHTTPPOST.html


namespace Remarkable.Models.Services
{
    public class SiteResourceService
    {
        public enum ForceOption
        {
            WIDTH,
            HEIGHT
        }

        public void SaveSiteResource(SiteResource sr)
        {
            using (var _db = new dbDataContext())
            {
                if (sr.ID == 0)
                {
                    sr.CreateDate = DateTime.Now;
                    _db.SiteResources.InsertOnSubmit(sr);
                }
                else
                {
                    var saveSr = _db.SiteResources.Single(x => x.ID == sr.ID);
                    saveSr.FileName = sr.FileName;
				    saveSr.Code = sr.Code;
				    saveSr.SiteResourceTypeE = sr.SiteResourceTypeE;
				    saveSr.Caption = sr.Caption;
				    saveSr.Caption2 = sr.Caption2;
				    saveSr.EmbedCode = sr.EmbedCode;
				    saveSr.Width = sr.Width;
				    saveSr.Height = sr.Height;
				    saveSr.FileSize = sr.FileSize;
				    saveSr.ParentID = sr.ParentID;
				    saveSr.ObjectID = sr.ObjectID;
				    saveSr.ObjectTypeE = sr.ObjectTypeE;
				    saveSr.OrderNo = sr.OrderNo;
				    saveSr.ContentID = sr.ContentID;
                    saveSr.ProductID = sr.ProductID;
                    saveSr.BrandID = sr.BrandID;
                }
                _db.SubmitChanges();
            }
        }

        public void DeleteSiteResource(SiteResource sr)
        {
            using (var _db = new dbDataContext())
            {
                var delSiteResource = from o in _db.SiteResources
                                      where o.ID == sr.ID
                                      select o;
                //delete the order
                _db.SiteResources.DeleteAllOnSubmit(delSiteResource);
                _db.SubmitChanges();
            }
        }

        public SiteResource CompleteTicket(SiteResourceTicket job, SiteResourceOwner owner)
        {
            var sr = new SiteResource()
            {
                Code = job.Code,
                ObjectID = owner.GetObjectID(),
                ObjectType = owner.GetObjectType(),
                SiteResourceType = job.SiteResourceType,
                FileName = job.FileName,
                Caption = job.Caption
            };

            switch(owner.GetObjectType())
            {
                case SiteResourceOwner.ObjectTypeOption.Content:
                    sr.ContentID = owner.GetObjectID();
                    break;
                case SiteResourceOwner.ObjectTypeOption.Product:
                    sr.ProductID = owner.GetObjectID();
                    break;
                case SiteResourceOwner.ObjectTypeOption.Brand:
                    sr.BrandID = owner.GetObjectID();
                    break;
            }

            SaveSiteResource(sr);

            var fromDir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            var topath = getFileDirectory(sr);
            var todir = new DirectoryInfo(topath);
            todir.Create();
            var files = fromDir.GetFiles().ToList();
            sr.FileSize = files.Single(x => x.Name == sr.FileName).Length;

            files.ForEach(x => {
                x.CopyTo(Path.Combine(topath, x.Name));
            });
            fromDir.Delete(true);
            
            if (sr.isImage())
            {
                System.Drawing.Image img = System.Drawing.Image.FromFile(Path.Combine(topath, getFileNameVersion(sr.FileName, SiteResource.SiteResourceVersion.ITEM_PAGE, false)));
                sr.Width = img.Width;
                sr.Height = img.Height;
            }
            
            SaveSiteResource(sr);

            return sr;
        }

        public SaveResult SaveJob(HttpPostedFileBase file, ISavedFile job)
        {
            SaveResult result = null;
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            if (!dir.Exists) dir.Create();
            job.FileName = PrepareName(dir.FullName, file.FileName, job.SiteResourceType);
            
            switch(job.SiteResourceType)
            {
                /*
                case SiteResource.SiteResourceTypeOption.Movie_Artwork:
                case SiteResource.SiteResourceTypeOption.high_res_JPEG:
                    result = PrepareImage(file, job);
                    break;
                    */
                default :
                    file.SaveAs(Path.Combine(dir.FullName, job.FileName));
                    result = new SaveResult() { success = true };
                    break;
            }
            result.job = job;
            return result;
        }

        private string PrepareName(string path, string name, SiteResource.SiteResourceTypeOption type, bool overwrite = false)
        {
            string extension = null;

            switch (type)
            {
                /*
                case SiteResource.SiteResourceTypeOption.Movie_Artwork:
                case SiteResource.SiteResourceTypeOption.high_res_JPEG:
                    extension = ".jpg";
                    break;
                    */
                default:
                    extension = Path.GetExtension(name);
                    break;
            }

            var savename = Path.GetFileNameWithoutExtension(name).ToSafeString(false, true) + extension;
            if (!overwrite && System.IO.File.Exists(Path.Combine(path, savename)))
            {
                return System.DateTime.Now.Ticks + "_" + savename;
            }
            return savename;
        }

        private SaveResult PrepareImage(HttpPostedFileBase file, ISavedFile job)
        {
            try
            {
                byte[] data;
                using (Stream inputStream = file.InputStream)
                {
                    MemoryStream memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    data = memoryStream.ToArray();
                }
                var imageResizer = new ImageResizer(data);

                var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
                if (!dir.Exists) dir.Create();
                job.FileName = PrepareName(dir.FullName, job.FileName, job.SiteResourceType);
                var savePath = HttpContext.Current.Server.MapPath(job.SavePath);
            
                switch (job.SiteResourceType)
                {
                    /*
                    case SiteResource.SiteResourceTypeOption.Movie_Artwork:
                        imageResizer.Resize(400, ImageEncoding.Jpg100);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_PAGE);
                        break;
                    case SiteResource.SiteResourceTypeOption.high_res_JPEG:
                        imageResizer.Resize(400, ImageEncoding.Jpg100);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_LIST);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(900, ImageEncoding.Jpg100);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_LARGE);

                        File.WriteAllBytes(Path.Combine(savePath, job.FileName), data);
                        break;
                        */
                    default :
                        break;
                }
                
            }
            catch (Exception e)
            {
                /*
                if (System.IO.File.Exists(fullPath))
                {
                    new System.IO.FileInfo(fullPath).Delete();
                }
                 * */
                return new SaveResult {success=false, error=e.Message };
            }
            return new SaveResult { success = true };
        }

        private byte[] compositeTextToImage(byte[] imageData, string text, int fontsize, int targetWidth)
        {
            Bitmap bitmap;
            Bitmap bitmapout;
            using (var ms = new MemoryStream(imageData))
            {
                bitmap = new Bitmap(ms);
                float scale = (float)targetWidth / bitmap.Width;
                int targetHeight = (int)(bitmap.Height * scale);
                bitmapout = new Bitmap(targetWidth, targetHeight);
                using (Graphics graphics = Graphics.FromImage(bitmapout))
                {
                    graphics.DrawImage(bitmap, new Rectangle(0, 0, bitmapout.Width, bitmapout.Height));

                    RectangleF drawRect = new RectangleF(0, 0, bitmapout.Width, bitmapout.Height);
                    Font drawFont = new Font("Arial", fontsize, FontStyle.Bold, GraphicsUnit.Pixel);

                    // Set format of string.
                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;
                    drawFormat.LineAlignment = StringAlignment.Center;

                    SolidBrush drawBrush = new SolidBrush(Color.FromArgb(200, 255, 0, 0));

                    graphics.DrawString(text, drawFont, drawBrush, drawRect, drawFormat);
                }

                ImageConverter converter = new ImageConverter();
                return (byte[])converter.ConvertTo(bitmapout, typeof(byte[]));
            }
        }

        private string saveImageResize(ImageResizer resizer, ISavedFile job, SiteResource.SiteResourceVersion version)
        {
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            var file = Path.Combine(dir.FullName, getFileNameVersion(job.FileName, version, false));
            resizer.SaveToFile(file);
            return file;
        }
      
        private static string getFileDirectory(SiteResource sR)
        {
            return HttpContext.Current.Server.MapPath("~/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + sR.ObjectID.ToString() + "/" + sR.Code + "/");
        }

        public void AddFromDisk(SiteResource sR, FileInfo file)
        {
            string imagesDir = HttpContext.Current.Server.MapPath("~/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + sR.ObjectID.ToString() + "/");
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(imagesDir);
            if (!dir.Exists)
                dir.Create();

            ///sR.FileName = ea.Controllers.SiteController.cleanFileName(sR.FileName);

            if (System.IO.File.Exists(imagesDir + sR.FileName))
            {
                //if the file is already there, then create a unique name.
                sR.FileName = System.DateTime.Now.Ticks + "_" + sR.FileName;
            }

            file.CopyTo(imagesDir + sR.FileName);
            SaveSiteResource(sR);
        }

        public void Delete(SiteResource sR)
        {
            var dir = new DirectoryInfo(getFileDirectory(sR));
            if (dir.Exists)
            {
                try
                {
                    dir.Delete(true);
                }
                catch { }
            }
            DeleteSiteResource(sR);
        }

        public void Delete(SiteResourceTicket ticket)
        {
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(ticket.SavePath));
            if (dir.Exists)
                dir.Delete(true);
        }

        public static string getUrl(SiteResource sR, SiteResource.SiteResourceVersion version = SiteResource.SiteResourceVersion.ITEM_PAGE, bool isRetina = false)
        {
            return useS3(sR, version) ? "/titles/file/" + sR.ID:
                getPathWithoutFile(sR, version) + getFileNameVersion(sR.FileName, version, isRetina);
        }
        

        public static string getFileNameVersion(string fileName, SiteResource.SiteResourceVersion version, bool isRetina = false)
        {
            if (isRetina)
            {
                switch (version)
                {
                    case SiteResource.SiteResourceVersion.ITEM_PAGE_RETINA: return fileName;
                    case SiteResource.SiteResourceVersion.ITEM_LIST_RETINA: return fileName.Replace("@2x", "_list@2x");
                }
                return "version not found";
            }
            else
            {
                switch (version)
                {
                    case SiteResource.SiteResourceVersion.ITEM_PAGE: return fileName;
                    case SiteResource.SiteResourceVersion.ITEM_LIST: return fileName.Replace(".", "_list.");
                    case SiteResource.SiteResourceVersion.ITEM_LARGE: return fileName.Replace(".", "_lrg.");
                }
                return "version not found";
            }
        }

        public static SiteResource.SiteResourceVersion getVersionByFileName(string fileName)
        {
            var fileWithoutExt = Path.GetFileNameWithoutExtension(fileName);
            if (fileWithoutExt.EndsWith("@2x"))
                return SiteResource.SiteResourceVersion.ITEM_PAGE_RETINA;
            else if (fileWithoutExt.EndsWith("_list@2x"))
                return SiteResource.SiteResourceVersion.ITEM_LIST_RETINA;
            else if (fileWithoutExt.EndsWith("_list"))
                return SiteResource.SiteResourceVersion.ITEM_LIST;
            else if (fileWithoutExt.EndsWith("_lrg"))
                return SiteResource.SiteResourceVersion.ITEM_LARGE;
            else
                return SiteResource.SiteResourceVersion.ITEM_PAGE;
        }

        public static string getPathWithoutFile(SiteResource sR, SiteResource.SiteResourceVersion version)
        {
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/" + sR.Code + "/";
        }

        public static string getS3Keypath(SiteResource sR)
        {
            return sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/" + sR.Code + "/" + sR.FileName;
        }

        public static string getS3MetaData(SiteResource sR)
        {
            return string.Format("{0} {1}-{2}", "Remarkable", sR.SiteResourceType, SiteResource.SiteResourceVersion.ITEM_PAGE);
        }

        public static bool useS3(SiteResource sR, SiteResource.SiteResourceVersion version)
        {
            return false;
            /*
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.Movie_Artwork:
                case SiteResource.SiteResourceTypeOption.Billboard_Desktop:
                case SiteResource.SiteResourceTypeOption.Billboard_Mobile:
                    return false;
                default:
                    switch (version)
                    {
                        case SiteResource.SiteResourceVersion.ITEM_PAGE:
                            return true;
                        default:
                            return false;
                    }
            }
            */
        }

        //TODO Remove when you have images
        public static string getPathDemo(SiteResource sR)
        {
            /*
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.HEADER :
                    return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
            }
            */
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/2/" + "th_ANXIETY_AID_30s_RGB.jpg";
            
        }

        public static string getThumbPath(SiteResource sR)
        {
            /*
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.CONTENT:
                    return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
                    
                case SiteResource.ResourceType.EXTERNAL_IMAGE:
                    return sR.FileName;
                    
            }
             * */
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
        }

        public string getFileName(SiteResource sR)
        {
            return sR.FileName;
        }


        public void SavePrep(System.Web.Mvc.ModelStateDictionary ModelState, SiteResourceOwner model, dbDataContext _db)
        {
            ModelState.Keys.Where(x => x.StartsWith("ResourceTickets")).ToList().ForEach(x => ModelState.Remove(x));
            model.Resources = _db.SiteResources.Where(x => x.ObjectID == model.GetObjectID() && x.ObjectTypeE == (int)model.GetObjectType()).ToList();
            model.ResourceTickets = model.ResourceTickets.Where(x => x.IsActive).ToList();
        }

        public SiteResourceContentReplacer SaveCommit(SiteResourceOwner model, SiteResourceOwner saveModel, dbDataContext _db)
        {
            var srcr = new SiteResourceContentReplacer();
            srcr.AddReplace("../../../../", "/");
            srcr.AddReplace("../../../", "/");
            srcr.AddReplace("../../", "/");
            srcr.AddReplace("../", "/");

            model.Resources.Where(x => model.ResourceTickets.Select(t => t.SiteResourceType).ToList().Contains(x.SiteResourceType)).Where(x => IsOnePerType(x.SiteResourceType)).ToList().ForEach(x => Delete(x));
            model.ResourceTickets.ForEach(x => {
                var sr = CompleteTicket(x, model);
                srcr.AddReplace(x.PreviewUrlLinkOnly, sr.getUrl());
            });
            return srcr;
        }


        /* Util Actions */

        public static bool IsOnePerType(SiteResource.SiteResourceTypeOption type)
        {
            return type != SiteResource.SiteResourceTypeOption.Product_Image;
        }


    }

    public class SaveResult
    {
        public bool success { get; set; }
        public string error { get; set; }
        public ISavedFile job { get; set; }
    }
}
