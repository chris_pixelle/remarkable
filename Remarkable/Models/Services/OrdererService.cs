﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remarkable.Models;

namespace Remarkable.Models.Services
{
    public class OrdererService
    {
        /// <summary>
        /// Check to make sure the ordered set has order numbers, if not it sets them in the order the items are provided.
        /// </summary>
        /// <param name="children">The items to be checked and set if needed.</param>
        /// <returns>Whether the children need to be saved or not.</returns>
        public static bool orderingCheck(List<IOrderedObject> children)
        {
            if(children.All(o => o.OrderNo == null))
            {
                int i = 1;
                foreach(var child in children)
                {
                    child.OrderNo = i;
                    i++;
                }
                return true;
            }
            return false;
        }

        public static void setNextOrderNo(IOrderedObject toSet,List<IOrderedObject> children)
        {
            IOrderedObject or = children.OrderBy(o => o.OrderNo).LastOrDefault();
            toSet.OrderNo = or == null ? 1 : or.OrderNo + 1;
        }

        public static List<IOrderedObject> moveUp(IOrderedObject toMove, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderBy(o => o.OrderNo).ToList();
            int i = 0;
            foreach(var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != 0)
                    {
                        int OrderNo = (int)children[i - 1].OrderNo;
                        children[i - 1].OrderNo = toMove.OrderNo;
                        toMove.OrderNo = OrderNo;
                        savers.Add(children[i - 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedObject> moveDown(IOrderedObject toMove, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderBy(o => o.OrderNo).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != children.Count-1)
                    {
                        int OrderNo = (int)children[i + 1].OrderNo;
                        children[i + 1].OrderNo = toMove.OrderNo;
                        toMove.OrderNo = OrderNo;
                        savers.Add(children[i + 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedObject> delete(IOrderedObject toDelete, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderByDescending(o => o.OrderNo).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toDelete.ID != obj.ID)
                {
                    obj.OrderNo = obj.OrderNo - 1;
                    savers.Add(obj);
                }
                else
                {
                    toDelete.OrderNo = null;
                    break;
                }
                i++;
            }
            return savers;
        }

    }

    public class OrdererASecondWayService
    {
        /// <summary>
        /// Check to make sure the ordered set has order numbers, if not it sets them in the order the items are provided.
        /// </summary>
        /// <param name="children">The items to be checked and set if needed.</param>
        /// <returns>Whether the children need to be saved or not.</returns>
        public static bool orderingCheck(List<IOrderedASecondWayObject> children)
        {
            if (children.All(o => o.OrderNo2 == null))
            {
                int i = 1;
                foreach (var child in children)
                {
                    child.OrderNo2 = i;
                    i++;
                }
                return true;
            }
            return false;
        }

        public static void setNextOrderNo(IOrderedASecondWayObject toSet, List<IOrderedASecondWayObject> children)
        {
            IOrderedASecondWayObject or = children.OrderBy(o => o.OrderNo2).LastOrDefault();
            toSet.OrderNo2 = or == null ? 1 : or.OrderNo2 + 1;
        }

        public static List<IOrderedASecondWayObject> moveUp(IOrderedASecondWayObject toMove, List<IOrderedASecondWayObject> children)
        {
            List<IOrderedASecondWayObject> savers = new List<IOrderedASecondWayObject>();
            children = children.OrderBy(o => o.OrderNo2).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != 0)
                    {
                        int OrderNo2 = (int)children[i - 1].OrderNo2;
                        children[i - 1].OrderNo2 = toMove.OrderNo2;
                        toMove.OrderNo2 = OrderNo2;
                        savers.Add(children[i - 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedASecondWayObject> moveDown(IOrderedASecondWayObject toMove, List<IOrderedASecondWayObject> children)
        {
            List<IOrderedASecondWayObject> savers = new List<IOrderedASecondWayObject>();
            children = children.OrderBy(o => o.OrderNo2).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != children.Count - 1)
                    {
                        int OrderNo2 = (int)children[i + 1].OrderNo2;
                        children[i + 1].OrderNo2 = toMove.OrderNo2;
                        toMove.OrderNo2 = OrderNo2;
                        savers.Add(children[i + 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedASecondWayObject> delete(IOrderedASecondWayObject toDelete, List<IOrderedASecondWayObject> children)
        {
            List<IOrderedASecondWayObject> savers = new List<IOrderedASecondWayObject>();
            children = children.OrderByDescending(o => o.OrderNo2).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toDelete.ID != obj.ID)
                {
                    obj.OrderNo2 = obj.OrderNo2 - 1;
                    savers.Add(obj);
                }
                else
                {
                    toDelete.OrderNo2 = null;
                    break;
                }
                i++;
            }
            return savers;
        }

    }
}