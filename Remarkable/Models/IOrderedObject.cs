﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remarkable.Models
{
    public interface IOrderedObject
    {
        int ID  { get; set; }
        int? OrderNo  { get; set; }
    }

    public interface IOrderedASecondWayObject
    {
        int ID { get; set; }
        int? OrderNo2 { get; set; }
    }
}