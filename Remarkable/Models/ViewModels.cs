﻿using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Remarkable.Models.Extensions;

namespace Remarkable.Models
{
    [MetadataType(typeof(Brand_Validation))]
    public partial class Brand: SiteResourceOwner
    {
        public override int GetObjectID() { return ID; }
        public override ObjectTypeOption GetObjectType() { return ObjectTypeOption.Brand; }
        public override List<SiteResource> GetResources() { return SiteResources.ToList(); }


        public List<Product> GetProducts()
        {
            return Products.ToList();
        }

        public string GetUrl()
        {
            return string.Format("/brands/{0}", NameUrl);
        }
    }

    public class Brand_Validation
    {
        [Required]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Meta Description")]
        [DataType(DataType.MultilineText)]
        public string MetaDescription { get; set; }

        [DisplayName("Hidden")]
        public bool IsHidden { get; set; }
    }

    [MetadataType(typeof(Product_Validation))]
    public partial class Product : SiteResourceOwner
    {
        public override int GetObjectID() { return ID; }
        public override ObjectTypeOption GetObjectType() { return ObjectTypeOption.Product; }
        public override List<SiteResource> GetResources() { return SiteResources.ToList(); }

        public List<Tag> Tags { get; set; }
        public List<Tag> GetTags()
        {
            return Tag_Products.Select(x => x.Tag).ToList();
        }


        public string GetUrl()
        {
            return string.Format("/products/{0}", NameUrl);
        }
    }

    public class Product_Validation
    {
        [Required]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Meta Description")]
        [DataType(DataType.MultilineText)]
        public string MetaDescription { get; set; }

        [DisplayName("Brand")]
        public bool BrandID { get; set; }

        [DisplayName("Hidden")]
        public bool IsHidden { get; set; }

        [DataType(DataType.MultilineText)]
        public string Details { get; set; }
    }

    [MetadataType(typeof(Content_Validation))]
    public partial class Content : SiteResourceOwner
    {
        public enum TypeOption
        {
            Site_Content,
            Home_Page_Banner,
            Home_Page_Promo,
            Home_Page_Large_Promo,
            News
        }

        public enum SystemTypeOption
        {
            StudWelding,
            About,
            Contact,
            MetalDeck,
            Health,
            Careers,
            News,
            Home,
            HomePagePromo
        }

        public TypeOption Type
        {
            get
            {
                return (TypeOption)TypeE;
            }
            set
            {
                TypeE = (int)value;
            }
        }

        public SystemTypeOption? SystemType
        {
            get
            {
                return (SystemTypeOption?)SystemTypeE;
            }
            set
            {
                SystemTypeE = (int?)value;
            }
        }

        public override int GetObjectID() { return ID; }
        public override ObjectTypeOption GetObjectType() { return ObjectTypeOption.Content; }
        public override List<SiteResource> GetResources() { return SiteResources.ToList(); }
    }

    public class Content_Validation
    {
        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Html1 { get; set; }

        [DisplayName("Meta Description")]
        [DataType(DataType.MultilineText)]
        public string MetaDescription { get; set; }

        [DisplayName("Hidden")]
        public bool IsHidden { get; set; }
    }

    public class HomePageViewModel
    {
        public List<Product> Products { get; set; }
        public List<Content> Banners { get; set; }
        public List<Content> Promotions { get; set; }
        public List<Content> Large_Promotions { get; set; }
    }

    public class MenuViewModel
    {
        public List<Brand> Brands { get; set; }
    }
}