﻿using System.ComponentModel.DataAnnotations;

namespace Remarkable.Models
{
    public class ContactModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        /*
        [Required]
        public string Phone { get; set; }
        */
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
    }
}