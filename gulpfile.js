var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');



gulp.task('zip-up-port', function() {
  return gulp.src('./emarkable/App_Data/Deployments/190122_Mvc5/*.sql')
    .pipe(concat('190122_Mvc5.sql'))
    .pipe(gulp.dest('./emarkable/App_Data/Deployments/190122_Mvc5/dest/'));
});

gulp.task('sass',function(){
  gulp.src('Remarkable/sass/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('Remarkable/Content'))
});  


gulp.task('sass:watch',function(){
  gulp.watch('Remarkable/sass/**/*.scss', ['sass']);
})

gulp.task('default', ['sass:watch']);