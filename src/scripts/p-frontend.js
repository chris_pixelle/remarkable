//Front end common functions
console.log('common functions here, prefix website specific js files with p-')
var multipleSwiper = new Swiper ('.multiple-product-swiper', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 3,
      spaceBetween: 10,
      loop: true,
   
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
	},
	breakpoints: {
        1024: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 0,
        },
	}

  })

  var productSwiper = new Swiper ('.product-swiper', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 1,
      spaceBetween: 10,
      loop: true,
   
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
  },

  pagination: {
    el: '.swiper-pagination',
    clickable: 'true',
  },

  })

  $( document ).ready(function() {
    $( ".wayfinder li").not(":last").append( "<p>/</p>" );
});